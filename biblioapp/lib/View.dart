

class View {
  final String imageDir;
  final String imageArr;
  final String text;

  View(this.imageDir, this.imageArr, this.text);
}