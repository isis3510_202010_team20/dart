import 'package:biblioapp/authentificate/registrer.dart';
import 'package:biblioapp/authentificate/signin.dart';
import 'package:flutter/material.dart';


class Auth extends StatefulWidget {
  @override
  _AuthState createState() => _AuthState();
}

class _AuthState extends State<Auth> {
  bool showSignIN = true;
  void toggleView(){
    setState(() => showSignIN = !showSignIN);//switch from false to true & true to false
  }

  @override
  Widget build(BuildContext context) {
    if(showSignIN){
      return SignIn(toggleView: toggleView);
    }else{
      return Register(toggleView: toggleView);
    }
  }
}

