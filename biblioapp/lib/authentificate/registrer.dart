import 'package:flutter/material.dart';
import 'package:biblioapp/services/auth.dart';
import 'package:biblioapp/shared/constants.dart';

class Register extends StatefulWidget {
  final Function toggleView;
  Register({this.toggleView});

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {

  final AuthServices _auth = AuthServices();
  final _formKey = GlobalKey<FormState>();

  String email = '';
  String password = '';
  String error = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor : Colors.lightBlue[100],
        appBar: AppBar(
          backgroundColor: Colors.blueGrey[400],
          elevation: 0.0,
          title: Text('Register to the library'),
          actions: <Widget>[
            FlatButton.icon(
                icon: Icon(Icons.person),
                label: Text('Sign in'),
                onPressed:(){
                  widget.toggleView();
                }
            )
          ],
        ),
        body: Container(
            padding : EdgeInsets.symmetric(vertical: 20.0, horizontal: 50.0), // remove from the sides
            child: Form(
                key: _formKey,
                child: ListView(
                    children: <Widget>[
                      SizedBox(height: 20.0),
                      TextFormField(
                        decoration: textInputDecoration.copyWith(hintText:'Email'),
                          validator: (val) => val.isEmpty ? 'Enter an email' : null,
                          onChanged: (val){
                            setState(() => email = val);

                          }
                      ),
                      SizedBox(height: 20.0),
                      TextFormField(
                        decoration: textInputDecoration.copyWith(hintText:'Password'),
                        obscureText: true,
                        validator: (val) => val.length < 6 ? 'Enter a password 6+ chars long' : null,
                        onChanged: (val){
                          setState(() => password = val);

                        },
                      ),
                      SizedBox(height: 20.0),
                      RaisedButton(
                          color: Colors.grey[400],
                          child: Text(
                            'Register',
                            style : TextStyle(color : Colors.white),

                          ),
                          onPressed: () async {
                            if(_formKey.currentState.validate()){
                              dynamic result = await _auth.registerWithEmailAndPassword(email, password);
                              if(result == null){
                                setState(() => error = 'please supply a valid email');


                              }
                            }
                          }
                      ),
                      SizedBox(height : 12.0),
                      Text(
                        error,
                        style: TextStyle(color: Colors.red, fontSize: 14.0),
                      )


                    ]
                )
            )
        )
    );
  }
}
