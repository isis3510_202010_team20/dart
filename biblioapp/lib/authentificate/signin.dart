import 'package:biblioapp/services/auth.dart';
import 'package:flutter/material.dart';
import 'package:biblioapp/shared/constants.dart';
import 'dart:io';


class  SignIn extends StatefulWidget {

  final Function toggleView;
  SignIn({this.toggleView});

  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  final AuthServices _auth = AuthServices();
  final _formKey = GlobalKey<FormState>();

  // text field state
  String email = '';
  String password = '';
  String error = '';


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor : Colors.lightBlue[100],
      appBar: AppBar(
        backgroundColor: Colors.blueGrey[400],
        elevation: 0.0,
        title: Text('Sign in to the library'),
        actions: <Widget>[
          FlatButton.icon(
            icon: Icon(Icons.person),
            label: Text('Register'),
            onPressed:(){
              widget.toggleView();
            }
          )
        ],
      ),
      body: Container(
        padding : EdgeInsets.symmetric(vertical: 20.0, horizontal: 50.0), // remove from the sides
        child: Form(
            key: _formKey,
            child: ListView(
              children: <Widget>[
                SizedBox(height: 20.0),
                TextFormField(
                  decoration: textInputDecoration.copyWith(hintText:'Email'),
                  validator: (val) => val.isEmpty ? 'Enter an email' : null,
                  onChanged: (val){
                    setState(() => email = val);

                  }
                ),
                SizedBox(height: 20.0),
                TextFormField(
                  decoration: textInputDecoration.copyWith(hintText:'Password'),
                  obscureText: true,
                  validator: (val) => val.length < 6 ? 'Enter a password 6+ chars long' : null,
                  onChanged: (val){
                    setState(() => password = val);

                  },
                ),
                SizedBox(height: 20.0),
                RaisedButton(
                  color: Colors.grey[400],
                  child: Text(
                    'Sign in',
                    style : TextStyle(color : Colors.white),

                  ),
                  onPressed: () async {

                    try {
                      final result = await InternetAddress.lookup('google.com');
                      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
                        print('connected');
                        if(_formKey.currentState.validate()){
                          dynamic result = await _auth.signInWithEmailAndPassword(email, password);
                          if(result == null){
                            setState(() => error = 'Could not sign in with those credentials');
                          }
                        }
                      }
                    } on SocketException catch (_) {
                      print('not connected');
                      setState(() => error = 'No hay conexión a internet por favor intente más tarde');
                    }
                  }
                ),
                SizedBox(height : 12.0),
                Text(
                  error,
                  style: TextStyle(color: Colors.red, fontSize: 14.0),
                )

              ]
            )
          )
        )
      );
  }
}
//child: RaisedButton(
//child: Text('Sign in anon'),
//onPressed: () async{
//dynamic result = await _auth.signInAnonymous();
//if (result == null){
//print('error signing in');

//}else {
//print('signed in');
//print(result.uid);
//}

//}
