import 'package:moor/moor.dart';
import 'package:moor_flutter/moor_flutter.dart';
// These imports are only needed to open the database

// Moor works by source gen. This file will all the generated code.
part 'moor_database.g.dart';

// this will generate a table called "comments" for us. The rows of that table will
// be represented by a class called "Comment".
class Comments extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get description => text().withLength(min: 6, max: 32)();
  DateTimeColumn get dueDate => dateTime().nullable()();
}

class Journeys extends Table {
  IntColumn get idJourney => integer().autoIncrement()();
  DateTimeColumn get startDateJourney => dateTime()();
  DateTimeColumn get endDateJourney => dateTime()();
  IntColumn get durationJourney => integer()();
}

class Sections extends Table {
  TextColumn get sectionName => text()();
  TextColumn get descriptionSection => text().nullable()();
  TextColumn get instructionSection => text().nullable()();
  @override
  Set<Column> get primaryKey => {sectionName};
}

// this annotation tells moor to prepare a database class that uses both of the
// tables we just defined. We'll see how to use that database class in a moment. , daos: [CommentDao,SectionDao]
@UseMoor(tables: [Comments,Sections,Journeys], daos: [CommentDao, SectionDao, JourneyDao])
class MyDatabase extends _$MyDatabase {
  // we tell the database where to store the data with this constructor
  MyDatabase() : // Specify the location of the database file
       super((FlutterQueryExecutor.inDatabaseFolder(
  path: 'db.sqlite',
  // Good for debugging - prints SQL in the console
  logStatements: true,
  ))){
    moorRuntimeOptions.dontWarnAboutMultipleDatabases = true;
  }



  // you should bump this number whenever you change or add a table definition. Migrations
  // are covered later in this readme.
  @override
  int get schemaVersion => 2; // bump because the tables have changed

  @override
  MigrationStrategy get migration => MigrationStrategy(
      onCreate: (Migrator m) {
        return m.createAll();
      },
      onUpgrade: (Migrator m, int from, int to) async {
        if (from == 1) {
          // we added the dueDate property in the change from version 1
          await m.addColumn(sections, sections.sectionName);
          await m.addColumn(sections, sections.instructionSection);
          await m.createTable(journeys);
        }
      },
      beforeOpen: (details) async {
        if (details.wasCreated) {
          await into(sections).insert(
              Section(
                sectionName: 'Cafeteria',
                descriptionSection: "Si quieres refrescarte y comer algo antes o despues de tu recorrido por la Biblioteca. Puedes pasar al lado de la Salida para probar unos ricos y deliciosos snacks! ",
                instructionSection: "Dirigete al piso 1, ve a la entrada de la biblioteca, Gira a tu derecha y avanza hasta encontrar un puesto de comida",
            ));
          await into(sections).insert(
              Section(
                  sectionName: 'Exposicion Segundo Piso',
                  descriptionSection: "Ven y conoce las diferentes exposiciones de arte e historia de las nuevas tendencias de Arte Moderno. Para llegar a estas sección puedes tomar las escaleras del primer piso que se encuentra al frente de la Entrada Principal",
                  instructionSection: "Si te encuentras en el primer piso dirigete a la entrada principal hasta el fondo hasta encontrar unas escaleras cercanas, Camina a tu izquierda y sube las escaleras que se encuentran al frente tuyo ",
              ));
          await into(sections).insert(
              Section(
                sectionName: 'Punto de Información',
                descriptionSection: "Si deseas conocer en detalle los servicios que ofrece la Biblioteca, puedes dirigirte al Punto de Información donde podrán resolver todas tus dudas. Esta sección se encuentra en el Primer Piso al frente de la Entrada Principal",
                instructionSection: "Dirigete a la entrada principal, Baja las escaleras que se encuentran al frente tuyo, Gira a tu izquierda y camina derecho hasta llegar a una recepción que dice Punto de Información",
              ));
          await into(sections).insert(
              Section(
                  sectionName: 'Sala de Conciertos',
                  descriptionSection: "Ven y escucha las interpretaciones musicales mas representativas del arte colombia. Esta sección se encuentra en el Segundo Piso al lado de la Exposición del Segundo Piso",
                  instructionSection: "Dirigete al punto de información, Camina hasta el fondo hasta encontrar unas escaleras amplias, Sube las escaleras que se encuentran al frente tuyo, Camina hasta el fondo hasta encontrar unas puertas grandes de Madera",
              ));
          await into(sections).insert(
              Section(
                  sectionName: 'Sala de Lectura General',
                  descriptionSection: "¿Deseas ver ejemplares que te engacharan a ti y a tus amigos o familia? Ven y descubre los diferentes y grandiosos que la Biblioteca tiene para tí. Esta sección se encuentra en el Segundo Piso cruzando al fondo las Salas de Exposiciones",
                  instructionSection: "Dirigete a la exposición del segundo piso, Camina hacia el fondo de la Exposición de Mariposas, siguiendo por el pasillo de la Galería de Arte, Camina a tu izquierda hasta te encuentres la entrada a una libreria que tiene un letrero que dice Bienvenido a la Sección de Lectura General",
              ));

        }
      },
  );
}

// Denote which tables this DAO can access
@UseDao(tables: [Comments])
class CommentDao extends DatabaseAccessor<MyDatabase> with _$CommentDaoMixin {
  final MyDatabase db;

  // Called by the MyDatabase class
  CommentDao(this.db) : super(db);

  Future<List<Comment>> getAllComments() => select(comments).get();

  Stream<List<Comment>> watchAllComments() {
    return (select(comments)
      ..orderBy([
            (c) => OrderingTerm(expression: c.description),
      ]))
        .watch();
  }
  Future insertComment(Insertable<Comment> comment) => into(comments).insert(comment);
  Future updateComment(Insertable<Comment> comment) => update(comments).replace(comment);
  Future deleteComment(Insertable<Comment> comment) => delete(comments).delete(comment);
}

// Denote which tables this DAO can access
@UseDao(tables: [Sections])
class SectionDao extends DatabaseAccessor<MyDatabase> with _$SectionDaoMixin {
  final MyDatabase db;

  // Called by the MyDatabase class
  SectionDao(this.db) : super(db);

  Stream<List<Section>> watchAllSections() {
    return (select(sections)
      ..orderBy([
            (s) => OrderingTerm(expression: s.sectionName),
      ]))
        .watch();
  }
  Future<List<Section>> getAllSections() => select(sections).get();
  Future insertSection(Insertable<Section> section) => into(sections).insert(section);
  Future updateSection(Insertable<Section> section) => update(sections).replace(section);
  Future deleteSection(Insertable<Section> section) => delete(sections).delete(section);

}

// Denote which tables this DAO can access
@UseDao(tables: [Journeys])
class JourneyDao extends DatabaseAccessor<MyDatabase> with _$JourneyDaoMixin {
  final MyDatabase db;

  // Called by the MyDatabase class
  JourneyDao(this.db) : super(db);

  Stream<List<Journey>> watchAllJourneys() {
    return (select(journeys)
      ..orderBy([
            (j) => OrderingTerm(expression: j.idJourney),
      ]))
        .watch();
  }
  Future<List<Journey>> getAllJourneys() => select(journeys).get();
  Future insertJourney(Insertable<Journey> journey) => into(journeys).insert(journey);
  Future updateJourney(Insertable<Journey> journey) => update(journeys).replace(journey);
  Future deleteJourney(Insertable<Journey> journey) => delete(journeys).delete(journey);
}


