// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'moor_database.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps, unnecessary_this
class Comment extends DataClass implements Insertable<Comment> {
  final int id;
  final String description;
  final DateTime dueDate;
  Comment({@required this.id, @required this.description, this.dueDate});
  factory Comment.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return Comment(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      description: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}description']),
      dueDate: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}due_date']),
    );
  }
  factory Comment.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return Comment(
      id: serializer.fromJson<int>(json['id']),
      description: serializer.fromJson<String>(json['description']),
      dueDate: serializer.fromJson<DateTime>(json['dueDate']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'description': serializer.toJson<String>(description),
      'dueDate': serializer.toJson<DateTime>(dueDate),
    };
  }

  @override
  CommentsCompanion createCompanion(bool nullToAbsent) {
    return CommentsCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      description: description == null && nullToAbsent
          ? const Value.absent()
          : Value(description),
      dueDate: dueDate == null && nullToAbsent
          ? const Value.absent()
          : Value(dueDate),
    );
  }

  Comment copyWith({int id, String description, DateTime dueDate}) => Comment(
        id: id ?? this.id,
        description: description ?? this.description,
        dueDate: dueDate ?? this.dueDate,
      );
  @override
  String toString() {
    return (StringBuffer('Comment(')
          ..write('id: $id, ')
          ..write('description: $description, ')
          ..write('dueDate: $dueDate')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode =>
      $mrjf($mrjc(id.hashCode, $mrjc(description.hashCode, dueDate.hashCode)));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is Comment &&
          other.id == this.id &&
          other.description == this.description &&
          other.dueDate == this.dueDate);
}

class CommentsCompanion extends UpdateCompanion<Comment> {
  final Value<int> id;
  final Value<String> description;
  final Value<DateTime> dueDate;
  const CommentsCompanion({
    this.id = const Value.absent(),
    this.description = const Value.absent(),
    this.dueDate = const Value.absent(),
  });
  CommentsCompanion.insert({
    this.id = const Value.absent(),
    @required String description,
    this.dueDate = const Value.absent(),
  }) : description = Value(description);
  CommentsCompanion copyWith(
      {Value<int> id, Value<String> description, Value<DateTime> dueDate}) {
    return CommentsCompanion(
      id: id ?? this.id,
      description: description ?? this.description,
      dueDate: dueDate ?? this.dueDate,
    );
  }
}

class $CommentsTable extends Comments with TableInfo<$CommentsTable, Comment> {
  final GeneratedDatabase _db;
  final String _alias;
  $CommentsTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _descriptionMeta =
      const VerificationMeta('description');
  GeneratedTextColumn _description;
  @override
  GeneratedTextColumn get description =>
      _description ??= _constructDescription();
  GeneratedTextColumn _constructDescription() {
    return GeneratedTextColumn('description', $tableName, false,
        minTextLength: 6, maxTextLength: 32);
  }

  final VerificationMeta _dueDateMeta = const VerificationMeta('dueDate');
  GeneratedDateTimeColumn _dueDate;
  @override
  GeneratedDateTimeColumn get dueDate => _dueDate ??= _constructDueDate();
  GeneratedDateTimeColumn _constructDueDate() {
    return GeneratedDateTimeColumn(
      'due_date',
      $tableName,
      true,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [id, description, dueDate];
  @override
  $CommentsTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'comments';
  @override
  final String actualTableName = 'comments';
  @override
  VerificationContext validateIntegrity(CommentsCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.id.present) {
      context.handle(_idMeta, id.isAcceptableValue(d.id.value, _idMeta));
    }
    if (d.description.present) {
      context.handle(_descriptionMeta,
          description.isAcceptableValue(d.description.value, _descriptionMeta));
    } else if (isInserting) {
      context.missing(_descriptionMeta);
    }
    if (d.dueDate.present) {
      context.handle(_dueDateMeta,
          dueDate.isAcceptableValue(d.dueDate.value, _dueDateMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Comment map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Comment.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(CommentsCompanion d) {
    final map = <String, Variable>{};
    if (d.id.present) {
      map['id'] = Variable<int, IntType>(d.id.value);
    }
    if (d.description.present) {
      map['description'] = Variable<String, StringType>(d.description.value);
    }
    if (d.dueDate.present) {
      map['due_date'] = Variable<DateTime, DateTimeType>(d.dueDate.value);
    }
    return map;
  }

  @override
  $CommentsTable createAlias(String alias) {
    return $CommentsTable(_db, alias);
  }
}

class Section extends DataClass implements Insertable<Section> {
  final String sectionName;
  final String descriptionSection;
  final String instructionSection;
  Section(
      {@required this.sectionName,
      this.descriptionSection,
      this.instructionSection});
  factory Section.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final stringType = db.typeSystem.forDartType<String>();
    return Section(
      sectionName: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}section_name']),
      descriptionSection: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}description_section']),
      instructionSection: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}instruction_section']),
    );
  }
  factory Section.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return Section(
      sectionName: serializer.fromJson<String>(json['sectionName']),
      descriptionSection:
          serializer.fromJson<String>(json['descriptionSection']),
      instructionSection:
          serializer.fromJson<String>(json['instructionSection']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'sectionName': serializer.toJson<String>(sectionName),
      'descriptionSection': serializer.toJson<String>(descriptionSection),
      'instructionSection': serializer.toJson<String>(instructionSection),
    };
  }

  @override
  SectionsCompanion createCompanion(bool nullToAbsent) {
    return SectionsCompanion(
      sectionName: sectionName == null && nullToAbsent
          ? const Value.absent()
          : Value(sectionName),
      descriptionSection: descriptionSection == null && nullToAbsent
          ? const Value.absent()
          : Value(descriptionSection),
      instructionSection: instructionSection == null && nullToAbsent
          ? const Value.absent()
          : Value(instructionSection),
    );
  }

  Section copyWith(
          {String sectionName,
          String descriptionSection,
          String instructionSection}) =>
      Section(
        sectionName: sectionName ?? this.sectionName,
        descriptionSection: descriptionSection ?? this.descriptionSection,
        instructionSection: instructionSection ?? this.instructionSection,
      );
  @override
  String toString() {
    return (StringBuffer('Section(')
          ..write('sectionName: $sectionName, ')
          ..write('descriptionSection: $descriptionSection, ')
          ..write('instructionSection: $instructionSection')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(sectionName.hashCode,
      $mrjc(descriptionSection.hashCode, instructionSection.hashCode)));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is Section &&
          other.sectionName == this.sectionName &&
          other.descriptionSection == this.descriptionSection &&
          other.instructionSection == this.instructionSection);
}

class SectionsCompanion extends UpdateCompanion<Section> {
  final Value<String> sectionName;
  final Value<String> descriptionSection;
  final Value<String> instructionSection;
  const SectionsCompanion({
    this.sectionName = const Value.absent(),
    this.descriptionSection = const Value.absent(),
    this.instructionSection = const Value.absent(),
  });
  SectionsCompanion.insert({
    @required String sectionName,
    this.descriptionSection = const Value.absent(),
    this.instructionSection = const Value.absent(),
  }) : sectionName = Value(sectionName);
  SectionsCompanion copyWith(
      {Value<String> sectionName,
      Value<String> descriptionSection,
      Value<String> instructionSection}) {
    return SectionsCompanion(
      sectionName: sectionName ?? this.sectionName,
      descriptionSection: descriptionSection ?? this.descriptionSection,
      instructionSection: instructionSection ?? this.instructionSection,
    );
  }
}

class $SectionsTable extends Sections with TableInfo<$SectionsTable, Section> {
  final GeneratedDatabase _db;
  final String _alias;
  $SectionsTable(this._db, [this._alias]);
  final VerificationMeta _sectionNameMeta =
      const VerificationMeta('sectionName');
  GeneratedTextColumn _sectionName;
  @override
  GeneratedTextColumn get sectionName =>
      _sectionName ??= _constructSectionName();
  GeneratedTextColumn _constructSectionName() {
    return GeneratedTextColumn(
      'section_name',
      $tableName,
      false,
    );
  }

  final VerificationMeta _descriptionSectionMeta =
      const VerificationMeta('descriptionSection');
  GeneratedTextColumn _descriptionSection;
  @override
  GeneratedTextColumn get descriptionSection =>
      _descriptionSection ??= _constructDescriptionSection();
  GeneratedTextColumn _constructDescriptionSection() {
    return GeneratedTextColumn(
      'description_section',
      $tableName,
      true,
    );
  }

  final VerificationMeta _instructionSectionMeta =
      const VerificationMeta('instructionSection');
  GeneratedTextColumn _instructionSection;
  @override
  GeneratedTextColumn get instructionSection =>
      _instructionSection ??= _constructInstructionSection();
  GeneratedTextColumn _constructInstructionSection() {
    return GeneratedTextColumn(
      'instruction_section',
      $tableName,
      true,
    );
  }

  @override
  List<GeneratedColumn> get $columns =>
      [sectionName, descriptionSection, instructionSection];
  @override
  $SectionsTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'sections';
  @override
  final String actualTableName = 'sections';
  @override
  VerificationContext validateIntegrity(SectionsCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.sectionName.present) {
      context.handle(_sectionNameMeta,
          sectionName.isAcceptableValue(d.sectionName.value, _sectionNameMeta));
    } else if (isInserting) {
      context.missing(_sectionNameMeta);
    }
    if (d.descriptionSection.present) {
      context.handle(
          _descriptionSectionMeta,
          descriptionSection.isAcceptableValue(
              d.descriptionSection.value, _descriptionSectionMeta));
    }
    if (d.instructionSection.present) {
      context.handle(
          _instructionSectionMeta,
          instructionSection.isAcceptableValue(
              d.instructionSection.value, _instructionSectionMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {sectionName};
  @override
  Section map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Section.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(SectionsCompanion d) {
    final map = <String, Variable>{};
    if (d.sectionName.present) {
      map['section_name'] = Variable<String, StringType>(d.sectionName.value);
    }
    if (d.descriptionSection.present) {
      map['description_section'] =
          Variable<String, StringType>(d.descriptionSection.value);
    }
    if (d.instructionSection.present) {
      map['instruction_section'] =
          Variable<String, StringType>(d.instructionSection.value);
    }
    return map;
  }

  @override
  $SectionsTable createAlias(String alias) {
    return $SectionsTable(_db, alias);
  }
}

class Journey extends DataClass implements Insertable<Journey> {
  final int idJourney;
  final DateTime startDateJourney;
  final DateTime endDateJourney;
  final int durationJourney;
  Journey(
      {@required this.idJourney,
      @required this.startDateJourney,
      @required this.endDateJourney,
      @required this.durationJourney});
  factory Journey.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return Journey(
      idJourney:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}id_journey']),
      startDateJourney: dateTimeType.mapFromDatabaseResponse(
          data['${effectivePrefix}start_date_journey']),
      endDateJourney: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}end_date_journey']),
      durationJourney: intType
          .mapFromDatabaseResponse(data['${effectivePrefix}duration_journey']),
    );
  }
  factory Journey.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return Journey(
      idJourney: serializer.fromJson<int>(json['idJourney']),
      startDateJourney: serializer.fromJson<DateTime>(json['startDateJourney']),
      endDateJourney: serializer.fromJson<DateTime>(json['endDateJourney']),
      durationJourney: serializer.fromJson<int>(json['durationJourney']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'idJourney': serializer.toJson<int>(idJourney),
      'startDateJourney': serializer.toJson<DateTime>(startDateJourney),
      'endDateJourney': serializer.toJson<DateTime>(endDateJourney),
      'durationJourney': serializer.toJson<int>(durationJourney),
    };
  }

  @override
  JourneysCompanion createCompanion(bool nullToAbsent) {
    return JourneysCompanion(
      idJourney: idJourney == null && nullToAbsent
          ? const Value.absent()
          : Value(idJourney),
      startDateJourney: startDateJourney == null && nullToAbsent
          ? const Value.absent()
          : Value(startDateJourney),
      endDateJourney: endDateJourney == null && nullToAbsent
          ? const Value.absent()
          : Value(endDateJourney),
      durationJourney: durationJourney == null && nullToAbsent
          ? const Value.absent()
          : Value(durationJourney),
    );
  }

  Journey copyWith(
          {int idJourney,
          DateTime startDateJourney,
          DateTime endDateJourney,
          int durationJourney}) =>
      Journey(
        idJourney: idJourney ?? this.idJourney,
        startDateJourney: startDateJourney ?? this.startDateJourney,
        endDateJourney: endDateJourney ?? this.endDateJourney,
        durationJourney: durationJourney ?? this.durationJourney,
      );
  @override
  String toString() {
    return (StringBuffer('Journey(')
          ..write('idJourney: $idJourney, ')
          ..write('startDateJourney: $startDateJourney, ')
          ..write('endDateJourney: $endDateJourney, ')
          ..write('durationJourney: $durationJourney')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      idJourney.hashCode,
      $mrjc(startDateJourney.hashCode,
          $mrjc(endDateJourney.hashCode, durationJourney.hashCode))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is Journey &&
          other.idJourney == this.idJourney &&
          other.startDateJourney == this.startDateJourney &&
          other.endDateJourney == this.endDateJourney &&
          other.durationJourney == this.durationJourney);
}

class JourneysCompanion extends UpdateCompanion<Journey> {
  final Value<int> idJourney;
  final Value<DateTime> startDateJourney;
  final Value<DateTime> endDateJourney;
  final Value<int> durationJourney;
  const JourneysCompanion({
    this.idJourney = const Value.absent(),
    this.startDateJourney = const Value.absent(),
    this.endDateJourney = const Value.absent(),
    this.durationJourney = const Value.absent(),
  });
  JourneysCompanion.insert({
    this.idJourney = const Value.absent(),
    @required DateTime startDateJourney,
    @required DateTime endDateJourney,
    @required int durationJourney,
  })  : startDateJourney = Value(startDateJourney),
        endDateJourney = Value(endDateJourney),
        durationJourney = Value(durationJourney);
  JourneysCompanion copyWith(
      {Value<int> idJourney,
      Value<DateTime> startDateJourney,
      Value<DateTime> endDateJourney,
      Value<int> durationJourney}) {
    return JourneysCompanion(
      idJourney: idJourney ?? this.idJourney,
      startDateJourney: startDateJourney ?? this.startDateJourney,
      endDateJourney: endDateJourney ?? this.endDateJourney,
      durationJourney: durationJourney ?? this.durationJourney,
    );
  }
}

class $JourneysTable extends Journeys with TableInfo<$JourneysTable, Journey> {
  final GeneratedDatabase _db;
  final String _alias;
  $JourneysTable(this._db, [this._alias]);
  final VerificationMeta _idJourneyMeta = const VerificationMeta('idJourney');
  GeneratedIntColumn _idJourney;
  @override
  GeneratedIntColumn get idJourney => _idJourney ??= _constructIdJourney();
  GeneratedIntColumn _constructIdJourney() {
    return GeneratedIntColumn('id_journey', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _startDateJourneyMeta =
      const VerificationMeta('startDateJourney');
  GeneratedDateTimeColumn _startDateJourney;
  @override
  GeneratedDateTimeColumn get startDateJourney =>
      _startDateJourney ??= _constructStartDateJourney();
  GeneratedDateTimeColumn _constructStartDateJourney() {
    return GeneratedDateTimeColumn(
      'start_date_journey',
      $tableName,
      false,
    );
  }

  final VerificationMeta _endDateJourneyMeta =
      const VerificationMeta('endDateJourney');
  GeneratedDateTimeColumn _endDateJourney;
  @override
  GeneratedDateTimeColumn get endDateJourney =>
      _endDateJourney ??= _constructEndDateJourney();
  GeneratedDateTimeColumn _constructEndDateJourney() {
    return GeneratedDateTimeColumn(
      'end_date_journey',
      $tableName,
      false,
    );
  }

  final VerificationMeta _durationJourneyMeta =
      const VerificationMeta('durationJourney');
  GeneratedIntColumn _durationJourney;
  @override
  GeneratedIntColumn get durationJourney =>
      _durationJourney ??= _constructDurationJourney();
  GeneratedIntColumn _constructDurationJourney() {
    return GeneratedIntColumn(
      'duration_journey',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns =>
      [idJourney, startDateJourney, endDateJourney, durationJourney];
  @override
  $JourneysTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'journeys';
  @override
  final String actualTableName = 'journeys';
  @override
  VerificationContext validateIntegrity(JourneysCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.idJourney.present) {
      context.handle(_idJourneyMeta,
          idJourney.isAcceptableValue(d.idJourney.value, _idJourneyMeta));
    }
    if (d.startDateJourney.present) {
      context.handle(
          _startDateJourneyMeta,
          startDateJourney.isAcceptableValue(
              d.startDateJourney.value, _startDateJourneyMeta));
    } else if (isInserting) {
      context.missing(_startDateJourneyMeta);
    }
    if (d.endDateJourney.present) {
      context.handle(
          _endDateJourneyMeta,
          endDateJourney.isAcceptableValue(
              d.endDateJourney.value, _endDateJourneyMeta));
    } else if (isInserting) {
      context.missing(_endDateJourneyMeta);
    }
    if (d.durationJourney.present) {
      context.handle(
          _durationJourneyMeta,
          durationJourney.isAcceptableValue(
              d.durationJourney.value, _durationJourneyMeta));
    } else if (isInserting) {
      context.missing(_durationJourneyMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {idJourney};
  @override
  Journey map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Journey.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(JourneysCompanion d) {
    final map = <String, Variable>{};
    if (d.idJourney.present) {
      map['id_journey'] = Variable<int, IntType>(d.idJourney.value);
    }
    if (d.startDateJourney.present) {
      map['start_date_journey'] =
          Variable<DateTime, DateTimeType>(d.startDateJourney.value);
    }
    if (d.endDateJourney.present) {
      map['end_date_journey'] =
          Variable<DateTime, DateTimeType>(d.endDateJourney.value);
    }
    if (d.durationJourney.present) {
      map['duration_journey'] = Variable<int, IntType>(d.durationJourney.value);
    }
    return map;
  }

  @override
  $JourneysTable createAlias(String alias) {
    return $JourneysTable(_db, alias);
  }
}

abstract class _$MyDatabase extends GeneratedDatabase {
  _$MyDatabase(QueryExecutor e) : super(SqlTypeSystem.defaultInstance, e);
  $CommentsTable _comments;
  $CommentsTable get comments => _comments ??= $CommentsTable(this);
  $SectionsTable _sections;
  $SectionsTable get sections => _sections ??= $SectionsTable(this);
  $JourneysTable _journeys;
  $JourneysTable get journeys => _journeys ??= $JourneysTable(this);
  CommentDao _commentDao;
  CommentDao get commentDao => _commentDao ??= CommentDao(this as MyDatabase);
  SectionDao _sectionDao;
  SectionDao get sectionDao => _sectionDao ??= SectionDao(this as MyDatabase);
  JourneyDao _journeyDao;
  JourneyDao get journeyDao => _journeyDao ??= JourneyDao(this as MyDatabase);
  @override
  Iterable<TableInfo> get allTables => allSchemaEntities.whereType<TableInfo>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities =>
      [comments, sections, journeys];
}

// **************************************************************************
// DaoGenerator
// **************************************************************************

mixin _$CommentDaoMixin on DatabaseAccessor<MyDatabase> {
  $CommentsTable get comments => db.comments;
}
mixin _$SectionDaoMixin on DatabaseAccessor<MyDatabase> {
  $SectionsTable get sections => db.sections;
}
mixin _$JourneyDaoMixin on DatabaseAccessor<MyDatabase> {
  $JourneysTable get journeys => db.journeys;
}
