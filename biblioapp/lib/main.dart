
import 'dart:io';

import 'package:biblioapp/services/auth.dart';
import 'package:biblioapp/data/moor_database.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'wrapper_menu.dart';
import 'ui/views/homepage.dart';
import 'package:biblioapp/models/user.dart';
import 'data/moor_database.dart';

class MyHttpOverrides extends HttpOverrides{
  @override
  HttpClient createHttpClient(SecurityContext context){
    return super.createHttpClient(context)
      ..badCertificateCallback = (X509Certificate cert, String host, int port)=> true;
  }
}

void main() {
  HttpOverrides.global = new MyHttpOverrides();
  runApp(new MyApp());
}
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          Provider(builder:(_) => MyDatabase().commentDao,),
          Provider(builder:(_) => MyDatabase().sectionDao,),
          Provider(builder:(_) => MyDatabase().journeyDao,),
          StreamProvider<User>.value(
            value: AuthServices().user,
          )
        ],
        child: Container(
          child: MaterialApp(
            home: Wrapper(),
          ),
        )
    );
  }
}





