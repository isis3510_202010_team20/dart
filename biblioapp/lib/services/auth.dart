import 'package:biblioapp/models/user.dart';
import 'package:biblioapp/services/database.dart';
import 'package:firebase_auth/firebase_auth.dart';

class AuthServices {
  //_auth private
  final FirebaseAuth _auth = FirebaseAuth.instance;

  //create user obj based on firebase user
  User _userFromFireBaseUser(FirebaseUser user){
    return user != null ? User(uid: user.uid) : null;
  }

  // auth change user stream
  Stream<User> get user {
    return _auth.onAuthStateChanged
        //.map((FirebaseUser user) => _userFromFireBaseUser(user));
        .map(_userFromFireBaseUser);
  }
  //sign in anonymous
  Future signInAnonymous() async {
    try{
      AuthResult result = await _auth.signInAnonymously();
      //use of the class users
      FirebaseUser user = result.user;
      return _userFromFireBaseUser(user);
    }catch(e){
      print(e.toString());
      return null;

    }
  }

  //sign in with email & password
  Future signInWithEmailAndPassword(String email, String password) async {
    try {
      AuthResult result = await _auth.signInWithEmailAndPassword(email: email, password: password);
      FirebaseUser user = result.user;
      return _userFromFireBaseUser(user);

    }catch(e){
      print(e.toString());
      return null;
    }
  }
  //register with email & password
  Future registerWithEmailAndPassword(String email, String password) async {
    try {
      AuthResult result = await _auth.createUserWithEmailAndPassword(email: email, password: password);
      FirebaseUser user = result.user;


  //resgister a name for the user whith the uid
      await DatabaseService(uid: user.uid).updateUserData('anon', 0);
      return _userFromFireBaseUser(user);


    }catch(e){
      print(e.toString());
      return null;
    }
  }

  //sign out
  Future signOut() async {
    try {
      return await _auth.signOut();

    }catch(e){
      print(e.toString());
      return null;

    }
  }
}