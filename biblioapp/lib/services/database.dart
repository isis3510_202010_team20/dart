
import 'package:biblioapp/models/User.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:biblioapp/models/user.dart';

class DatabaseService {

  final String uid;
 

  DatabaseService({ this.uid});

  // collection reference
  final CollectionReference usercollec = Firestore.instance.collection('usr');
  
  //final CollectionReference comments = Firestore.instance.collection('$seccion/Commentatios/$docID');

  Future<void> updateUserData(String name, int age) async {
    return await usercollec.document(uid).setData({
      
      'name': name,
      'age' : age,
      
    });
  }


  // user data from snapshots
  UserData _userDataFromSnapshot(DocumentSnapshot snapshot) {
    return UserData(
      uid: uid,
      name: snapshot.data['name'],
      age: snapshot.data['age'],
    );
  }


  //userlist from snapshot
  List<Userdat> _usrlistformSnapshot(QuerySnapshot snapshot){
    return snapshot.documents.map((doc){
      return Userdat(
        name : doc.data['name'] ?? '',
        age: doc.data['age'] ?? 0,
      );
    }).toList();
  }



Stream<List<Userdat>> get usr {
  return usercollec.snapshots().map(_usrlistformSnapshot);
  
}


 
  // get user doc stream
Stream<UserData> get userData {
    return usercollec.document(uid).snapshots()
      .map(_userDataFromSnapshot);
    }



}

  