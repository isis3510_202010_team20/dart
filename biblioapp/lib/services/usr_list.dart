import 'package:biblioapp/models/User.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'usr_tile.dart';

class UserList extends StatefulWidget {
  @override
  _UserListState createState() => _UserListState();
}

class _UserListState extends State<UserList> {
  @override
  Widget build(BuildContext context) {

    final usrs = Provider.of<List<Userdat>>(context) ?? [];
    usrs.forEach((usr) {
      print(usr.name);
      print(usr.age);
     });

    return ListView.builder(
      itemCount: usrs.length,
      itemBuilder:(context,index){
        return UserTile(usr: usrs[index] );
      }
      
    );
  }
}