//import 'dart:html';

import 'package:flutter/material.dart';
import '../models/User.dart';

class UserTile extends StatelessWidget {

  final Userdat usr;
  UserTile({this.usr});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: Card(
        margin: EdgeInsets.fromLTRB(20.0, 6.0, 20.0, 0.0),
        child: ListTile(
          leading: CircleAvatar(
            child: Icon(Icons.person),
          ),
          title: Text(usr.name),
          subtitle: Text("$usr.age"),
        ),
      ),
    );
  }
}