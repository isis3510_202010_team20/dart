import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import '../data/moor_database.dart';
import 'widget/new_comment_input.dart';

class CommentPlace extends StatefulWidget {

  CommentPlace({this.docID});
  final String docID;
  @override
  _CommentPlaceState createState() => _CommentPlaceState(doc: docID);
}

class _CommentPlaceState extends State<CommentPlace> {
  _CommentPlaceState({this.doc});
  final String  doc;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Comments'),
        ),
        body: Column(
          children: <Widget>[
            Expanded(child: _buildCommentList(context)),
            NewCommentInput(),//doc_cur: doc),
          ],
        ));
  }

  StreamBuilder<List<Comment>> _buildCommentList(BuildContext context) {
    final dao = Provider.of<CommentDao>(context);
    return StreamBuilder(
      stream: dao.watchAllComments(),
      builder: (context, AsyncSnapshot<List<Comment>> snapshot) {
        final comments = snapshot.data ?? List();

        return ListView.builder(
          itemCount: comments.length,
          itemBuilder: (_, index) {
            final itemComment = comments[index];
            return _buildListItem(itemComment, dao);
          },
        );
      },
    );
  }

  Widget _buildListItem(Comment itemComment, CommentDao dao) {
    return Slidable(
      actionPane: SlidableDrawerActionPane(),
      secondaryActions: <Widget>[
        IconSlideAction(
          caption: 'Delete',
          color: Colors.red,
          icon: Icons.delete,
          onTap: () => dao.deleteComment(itemComment),
        )
      ],
      child: CheckboxListTile(
        title: Text(itemComment.description),
        subtitle: Text('No subtitle'),
        value: true,
      ),
    );
  }
}