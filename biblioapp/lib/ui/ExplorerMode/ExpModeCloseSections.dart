import 'dart:async';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:moor_flutter/moor_flutter.dart' hide Column;
import 'package:provider/provider.dart';
import 'package:biblioapp/ui/ExplorerMode/Instructions.dart';
import 'package:biblioapp/data/moor_database.dart';

import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'ExplorerMode.dart';
import 'Instructions.dart';


class ExpModeCloseSections extends StatefulWidget {

  final String actualSection;
  final DateTime init;

  ExpModeCloseSections({this.actualSection, this.init});

  @override
  _ExpModeCloseSections createState() => _ExpModeCloseSections();
}

class _ExpModeCloseSections extends State<ExpModeCloseSections> {

  void handleInsertSection(DocumentSnapshot document) {
    print("inserting Section");
    var inst = "";
    for(var i = 0; i < document.data["instrucciones"].length; i++){
      inst += document.data["instrucciones"][i];
    }
    final section = SectionsCompanion(
      sectionName: Value(document.documentID),
      instructionSection: Value(inst),
    );
    final dao = Provider.of<SectionDao>(context);
    Future<List<Section>> a = dao.getAllSections();
    a.then((value) => print("handle insert Section ${value.toSet().contains(section)}"));
    /*final section = SectionsCompanion(
      sectionName: Value(document.documentID),
      instructionSection: Value(inst),
    );
    dao.insertSection(section);*/
  }

  Widget _buildListItem(BuildContext context, DocumentSnapshot document){
    print('hereeee ${document.data}');

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        GestureDetector(
          onTap: () {
            handleInsertSection(document);
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        InstructionsClass(
                          instructionSnapShot: document,
                          init: widget.init,
                        )
                ),
                ModalRoute.withName("/Instructions")
            );
          },
          child: Text(
            document.documentID,
            textDirection: TextDirection.ltr,
            textAlign: TextAlign.left,
            style: TextStyle(
              fontSize: 20,
              color: Color(0xff436b9a),
            ),
          ),
        ),
      ],
    );
  }

  Future<bool> _onBackPressed() {
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return CupertinoAlertDialog(
            title: Text(
                "Salir del modo recorrido"
            ),
            content: Text(
                "¿Estas seguro de salir del modo recorrido?"
            ),
            actions: <Widget>[
              CupertinoDialogAction(
                child: Text("No"),
                onPressed: () {
                  Navigator.pop(context,false);
                },
              ),
              CupertinoDialogAction(
                child: Text("Si"),
                onPressed: () {
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              ExplorerMode(
                                title: 'GUIAPP',
                                sectionName: widget.actualSection,
                              )
                      ),
                      ModalRoute.withName("/FinishTourMode")
                  );
                },
              )
            ],
          );
        }
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        appBar: AppBar(
          title: Text('GUIAPP Luis Angel'),
          backgroundColor: Color(0xff000000),
        ),
        body: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Text(
                        'Ubicación Actual',
                        textDirection: TextDirection.ltr,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontSize: 24,
                          color: Color(0xff385b84),
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Text(
                        widget.actualSection,
                        textDirection: TextDirection.ltr,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontSize: 20,
                          color: Color(0xff436b9a),
                        ),
                      ),
                    ],
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Text(
                    '¿A donde quieres ir?',
                    textDirection: TextDirection.ltr,
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: 20,
                      color: Color(0xff436b9a),
                    ),
                  ),
                ],
              ),
              Expanded(
                child: StreamBuilder(
                    stream: Firestore.instance.collection("secciones/${widget.actualSection}/seccionesCercanas").snapshots(), //Firestore.instance.collection("secciones").snapshots()
                    builder: (context, snapshot) {
                      if (!snapshot.hasData) return const Text("Cargando Secciones Cercanas");
                      print("snapshot close sections ${snapshot.data.documents}");
                      print("section to look: ${widget.actualSection}");
                      return ListView.builder(
                        itemExtent: 80.0,
                        itemCount: snapshot.data.documents.length,
                        itemBuilder: (context, index) =>
                            _buildListItem(context, snapshot.data.documents[index]),
                      );
                    }),
              )
            ],
          ),
        ),
      ),
    );
  }
}