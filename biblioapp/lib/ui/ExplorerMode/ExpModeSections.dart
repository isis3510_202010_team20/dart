import 'dart:async';
import 'dart:io';
import 'package:biblioapp/ui/ExplorerMode/ExpModeCloseSections.dart';
import 'package:flutter/cupertino.dart';
import 'package:moor_flutter/moor_flutter.dart' hide Column;
import 'package:provider/provider.dart';
import 'package:biblioapp/ui/ExplorerMode/Instructions.dart';
import 'package:biblioapp/data/moor_database.dart';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'Instructions.dart';


class ExpModeSections extends StatefulWidget {
  @override
  _ExpModeSections createState() => _ExpModeSections();
}

class _ExpModeSections extends State<ExpModeSections> {
  String _connectionStatus = 'Unknown';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;

  @override
  void initState() {
    super.initState();
    initConnectivity();
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Widget _buildListItem(BuildContext context, DocumentSnapshot document){

    return Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          GestureDetector(
            onTap: () {
              var now = new DateTime.now();
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => ExpModeCloseSections(actualSection: document.documentID, init: now,)),
              );
            },
            child: Text(
              document['nombre'],
              textDirection: TextDirection.ltr,
              textAlign: TextAlign.left,
              style: TextStyle(
                fontSize: 20,
                color: Color(0xff436b9a),
              ),
            ),
          ),
        ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('GUIAPP Luis Angel'),
          backgroundColor: Color(0xff000000),
        ),
        body: _connectionStatus == 'ConnectivityResult.none' ?
        ExpModeSectionsWithoutConnection():
        StreamBuilder(
            stream: Firestore.instance.collection("secciones").snapshots(),
            builder: (context, snapshot) {
              if (!snapshot.hasData) return const Text("Cargando Secciones");
              return ListView.builder(
                itemExtent: 80.0,
                itemCount: snapshot.data.documents.length,
                itemBuilder: (context, index) =>
                    _buildListItem(context, snapshot.data.documents[index]),
              );
            }),
    );
  }
  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        String wifiName, wifiBSSID, wifiIP;

        try {
          if (Platform.isIOS) {
            LocationAuthorizationStatus status =
            await _connectivity.getLocationServiceAuthorization();
            if (status == LocationAuthorizationStatus.notDetermined) {
              status =
              await _connectivity.requestLocationServiceAuthorization();
            }
            if (status == LocationAuthorizationStatus.authorizedAlways ||
                status == LocationAuthorizationStatus.authorizedWhenInUse) {
              wifiName = await _connectivity.getWifiName();
            } else {
              wifiName = await _connectivity.getWifiName();
            }
          } else {
            wifiName = await _connectivity.getWifiName();
          }
        } on PlatformException catch (e) {
          print(e.toString());
          wifiName = "Failed to get Wifi Name";
        }

        try {
          if (Platform.isIOS) {
            LocationAuthorizationStatus status =
            await _connectivity.getLocationServiceAuthorization();
            if (status == LocationAuthorizationStatus.notDetermined) {
              status =
              await _connectivity.requestLocationServiceAuthorization();
            }
            if (status == LocationAuthorizationStatus.authorizedAlways ||
                status == LocationAuthorizationStatus.authorizedWhenInUse) {
              wifiBSSID = await _connectivity.getWifiBSSID();
            } else {
              wifiBSSID = await _connectivity.getWifiBSSID();
            }
          } else {
            wifiBSSID = await _connectivity.getWifiBSSID();
          }
        } on PlatformException catch (e) {
          print(e.toString());
          wifiBSSID = "Failed to get Wifi BSSID";
        }

        try {
          wifiIP = await _connectivity.getWifiIP();
        } on PlatformException catch (e) {
          print(e.toString());
          wifiIP = "Failed to get Wifi IP";
        }

        setState(() {
          _connectionStatus = '$result\n'
              'Wifi Name: $wifiName\n'
              'Wifi BSSID: $wifiBSSID\n'
              'Wifi IP: $wifiIP\n';
        });
        break;
      case ConnectivityResult.mobile:
      case ConnectivityResult.none:
        setState(() => _connectionStatus = result.toString());
        break;
      default:
        setState(() => _connectionStatus = 'Failed to get connectivity.');
        break;
    }
  }
}

class ExpModeSectionsWithoutConnection extends StatefulWidget {

  @override
  _ExpModeSectionsWithoutConnection createState() => _ExpModeSectionsWithoutConnection();
}

class _ExpModeSectionsWithoutConnection extends State<ExpModeSectionsWithoutConnection> {

  Widget _buildListItemWithoutConnection(Section itemSection, SectionDao dao){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            GestureDetector(
              onTap: () {
                print("redirigir");
                print(itemSection.sectionName);
                print(itemSection.descriptionSection);
                print(itemSection.instructionSection);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => InstructionsClassWithoutConnection(itemSection: itemSection,init: DateTime.now(),)),
                );
              },
              child: Text(
                itemSection.sectionName,
                textDirection: TextDirection.ltr,
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontSize: 20,
                  color: Color(0xff436b9a),
                ),
              ),
            )
          ],
        ),

      ],
    );
  }


  @override
  Widget build(BuildContext context){
    final dao = Provider.of<SectionDao>(context);
    return Container(
      child: Column(
        children: <Widget>[
          Row(
            crossAxisAlignment:CrossAxisAlignment.center,
            children: <Widget>[
              Icon(
                Icons.cloud_off,
              ),
              Expanded(
                child: Text(
                  "Al parecer no estas conectado a Internet, sin embargo puedes ver las secciones que has visitado ultimamente",
                  textDirection: TextDirection.ltr,
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontSize: 20,
                    color: Color(0xff436b9a),
                  ),
                )
                ,
              )
            ],
          ),
          Expanded(
            child: StreamBuilder(
              stream: dao.watchAllSections(),
              builder: (context, AsyncSnapshot<List<Section>> snapshot) {
                final sections = snapshot.data ?? List();
                return ListView.separated(
                  itemCount: sections.length,
                  separatorBuilder: (BuildContext context, int index) => Divider(),
                  itemBuilder: (_, index) {
                    final itemSection = sections[index];
                    return _buildListItemWithoutConnection(itemSection, dao);
                  },
                );
              },
            ),
          )
        ],
      ),
    );

  }

}



