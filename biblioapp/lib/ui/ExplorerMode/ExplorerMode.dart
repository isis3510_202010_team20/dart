
import 'package:biblioapp/services/auth.dart';
import 'package:biblioapp/ui/ExplorerMode/ExpModeCloseSections.dart';
import 'package:biblioapp/ui/ExplorerMode/ExpModeSections.dart';
import 'package:biblioapp/data/moor_database.dart';
import 'package:biblioapp/models/user.dart';
import 'package:biblioapp/shared/loading.dart';
import 'package:biblioapp/services/usr_list.dart';
import 'package:biblioapp/ui/Statistics/StaticticsView.dart';
import 'package:biblioapp/ui/StudyTables/study_tables.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import '../../data/moor_database.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:provider/provider.dart';
import '../../services/database.dart';
import '../../models/User.dart';
import '../settings.dart';

class ExplorerMode extends StatefulWidget {
  ExplorerMode({Key key, this.title, this.sectionName}) : super(key: key);

  final String title;
  final String sectionName;

  @override
  _ExplorerModeState createState() => _ExplorerModeState();
}


class _ExplorerModeState extends State<ExplorerMode> {
  PageController pageController;
  final AuthServices _auth = AuthServices();
  @override
  void initState() {
    super.initState();
    pageController = PageController(initialPage: 1, viewportFraction: 0.6);
  }

  StreamBuilder<List<Journey>> _buildJourneys(BuildContext context) {
    final dao = Provider.of<JourneyDao>(context);
    return StreamBuilder(
      stream: dao.watchAllJourneys(),
      builder: (context, AsyncSnapshot<List<Journey>> snapshot) {
        final journeys = snapshot.data ?? List();
        return PageView.builder(
          itemCount: journeys.length,
          itemBuilder: (_, index) {
            final itemJourney = journeys[index];
            return imageSlider(index, itemJourney);
          },
        );
      },
    );
  }

  void _showSettingsPanel() {
    showModalBottomSheet(context: context, builder: (context) {
      return Container(
        padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 60.0),
        child: SettingsForm(),
      );
    });
  }


  @override
  Widget build(BuildContext context) {

    final user = Provider.of<User>(context);
    final s = Provider.of<SectionDao>(context);

    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
          backgroundColor: Color(0xFF0e263d),
        ),
        drawer: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: [
              DrawerHeader(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topRight,
                      end: Alignment.bottomLeft,
                      colors: [
                        Color(0xFF0e263d),
                        Color(0xFF2e5680)
                      ]
                  )
                ),
                child: Text(
                  'Guiapp',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 24,
                  ),
                ),
              ),
              ListTile(
                leading: Icon(Icons.score),
                title: Text("Estadisticas"),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => StatisticsClass(),
                    ),
                  );
                },
              ),
              ListTile(
                leading: Icon(Icons.local_parking),
                title: Text('Parqueadero'),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => StudyTables()),
                  );
                },
              ),
              ListTile(
                leading: Icon(Icons.settings),
                title: Text('Configuración'),
                onTap: () {
                  _showSettingsPanel();
                },
              ),
              ListTile(
                leading: Icon(Icons.exit_to_app),
                title: Text("Cerrar Sesión"),
                onTap : () {
                  //print("${_auth.signOut()}");
                  _auth.signOut();
                },
              ),
            ],
          ),
        ),
        body: Material(
          child: SingleChildScrollView(
            child: SizedBox(
              height: MediaQuery.of(context).size.height,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Stack(
                    children: <Widget>[
                      Image(
                        image: AssetImage('assets/images/tour_mode.jpg'),
                      ), //https://images.adsttc.com/media/images/51ed/45fe/e8e4/4e67/1700/010e/slideshow/2_(11).jpg?1374504433
                      Positioned(
                        width: MediaQuery.of(context).size.width,
                        top: MediaQuery.of(context).size.width * 0.05,
                        left: MediaQuery.of(context).size.height * 0.02,
                        //child : UserList(),
                        child:
                        StreamBuilder<UserData>(
                          stream: DatabaseService(uid: user.uid).userData,
                          builder: (context, snapshot) {
                            if (snapshot.hasData){
                            UserData userData = snapshot.data;
                            return Text(
                                'Hola ' + userData.name,
                                style: TextStyle(fontSize: 19.0, color: Color(0xffffffff),
                                fontWeight: FontWeight.bold));
                          }else{
                              return Text(
                                'Hola username',
                                style: TextStyle(fontSize: 19.0, color: Color(0xffffffff),
                                fontWeight: FontWeight.bold)
                              );
                          }},
                        )
                        /* child: Text(
                          'Hola UserList()',
                          style: TextStyle(
                            fontSize: 19.0,
                            color: Color(0xffffffff"),
                            fontWeight: FontWeight.bold,
                          ),
                        ), */
                      ),
                      Positioned(
                        width: MediaQuery.of(context).size.width,
                        top: MediaQuery.of(context).size.width * 0.12,
                        left: MediaQuery.of(context).size.height * 0.02,
                        child: Row(
                          children: [
                            Column(
                              children: [
                                Icon(
                                  Icons.place,
                                  color: Color(0xffffffff),
                                )
                              ],
                            ),
                            Column(
                              children: [
                                Text(
                                  widget.sectionName == null ? "Entrada" : widget.sectionName,
                                  style: TextStyle(
                                    fontSize: 18.0,
                                    color: Color(0xffffffff),
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                      Positioned(
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height,
                        top: MediaQuery.of(context).size.width * 0.30,
                        left: MediaQuery.of(context).size.height * 0.18,
                        child: Text(
                          'Has explorado',
                          style: TextStyle(
                            fontSize: 18.0,
                            color: Color(0xffffffff),
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Positioned(
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height,
                        top: MediaQuery.of(context).size.width * 0.48,
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                percentageVisits(context,s),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  'De la Biblioteca Luis Angel Arango',
                                  style: TextStyle(
                                    fontSize: 18.0,
                                    color: Color(0xffffffff),
                                    fontWeight: FontWeight.bold,
                                  ),
                                )
                              ],
                            )
                          ],
                        )
                      ),
                    ],
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 20.0, bottom: 20.0, left: 20.0),
                    alignment: Alignment.centerLeft,
                    child: Text(
                      'Ultimos Recorridos',
                      style: TextStyle(
                        fontSize: 15.0,
                        color: Color(0xff464646),
                        fontWeight: FontWeight.bold,
                      ),

                    ),
                  ),
                  Expanded(
                    child: _buildJourneys(context),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 20.0, bottom: 20.0, left: 20.0),
                    alignment: Alignment.centerLeft,
                    child: Text(
                      'Servicios',
                      style: TextStyle(
                        fontSize: 15.0,
                        color: Color(0xff464646),
                        fontWeight: FontWeight.bold,
                      ),

                    ),
                  ),
                  Row(
                    children: [
                      Spacer(), // Defaults to a flex of one.
                      Column(
                        children: [
                          GestureDetector(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => StudyTables()),
                              );
                            }, // handle your image tap here
                            child: Image(
                              image: AssetImage('assets/images/MesasEstudio.png'),
                              width: 150.0,
                              height: 150.0,
                            ),
                          )
                        ],
                      ),
                      Spacer(),
                    ],
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 20.0, bottom: 20.0, left: 20.0),
                    alignment: Alignment.centerLeft,
                    child: Text(
                      'Top Visitantes',
                      style: TextStyle(
                        fontSize: 15.0,
                        color: Color(0xff464646),
                        fontWeight: FontWeight.bold,
                      ),

                    ),
                  ),
                  SizedBox(
                    height: 200.0,
                    child:
                    Container(
                      child: StreamProvider<List<Userdat>>.value(
                          value: DatabaseService().usr,
                          child: UserList()),
                    ),
                  ),
                
                ],
            ),
            ),
          ),
        ),
        
        floatingActionButton: FloatingActionButton(
          backgroundColor: Color(0xFF0e263d),
          elevation: 0.0,
          onPressed: () {
            var now = new DateTime.now();
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        ExpModeCloseSections(
                          actualSection: widget.sectionName == null ? "Entrada" :widget.sectionName,
                          init: now,
                        )
                ),
                ModalRoute.withName("/ExpModeCloseSections")
            );
          },
          tooltip: 'Toggle',
          child: Icon(
            Icons.directions_run,
          ),
        ),
      );
  }

  percentageVisits(BuildContext ctx, SectionDao s){
    var l = s.getAllSections();

    return FutureBuilder<List<Section>>(
      future: l,
      builder: (ctx, AsyncSnapshot<List<Section>> snapshot) {
        if(snapshot.hasData)
          return Text(
              "${(snapshot.data.length * 6.25).toString()} %",
              style: TextStyle(
                fontSize: 18.0,
                color: Color(0xffffffff),
                fontWeight: FontWeight.bold,
              ),
          );
        return Text("0 %",style: TextStyle(
          fontSize: 18.0,
          color: Color(0xffffffff),
          fontWeight: FontWeight.bold,
        ),);
      },
    );
  }

  imageSlider(int index, Journey itemJourney){
      return Container(
          width: 100,
          height: 100,
          margin: EdgeInsets.all(5),
          child: Stack(
            children: [
              Center(
                child: Image(
                  image: AssetImage('assets/images/logo.jpeg'),
                  fit: BoxFit.cover,
                ),
              ),
              Center(
                child: Text(
                    itemJourney.idJourney.toString(),
                    style: TextStyle(fontSize: 19.0, color: Color(0xffffffff)),
                ),
              )
            ]
          )
      );
  }
}





