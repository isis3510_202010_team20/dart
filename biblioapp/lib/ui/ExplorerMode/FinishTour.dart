import 'package:biblioapp/ui/ExplorerMode/ExplorerMode.dart';
import 'package:flutter/material.dart';

class FinishTourMode extends StatefulWidget {
  FinishTourMode({Key key, this.section, this.duration}) : super(key: key);
  final String section;
  final Duration duration;
  @override
  _FinishTourModeState createState() => _FinishTourModeState();
}

class _FinishTourModeState extends State<FinishTourMode> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('GUIAPP Luis Angel'),
          backgroundColor: Color(0xff000000),
        ),
        body: GestureDetector(
          onTap: (){
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        ExplorerMode(
                          title: 'GUIAPP',
                          sectionName: widget.section,
                        )
                ),
                ModalRoute.withName("/Home")
            );
          },
          child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            color: Color(0xff8af0c5),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                Row(
                  children: <Widget>[
                    Text(
                      '¡Has llegado!',
                      textDirection: TextDirection.ltr,
                      style: TextStyle(
                        fontSize: 38,
                        color: Color(0xff0c2942),
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Text(
                        widget.section == null ? 'Has completado tu recorrido por la Biblioteca' : 'Has completado tu recorrido por la ${widget.section}',
                        textDirection: TextDirection.ltr,
                        style: TextStyle(
                          fontSize: 20,
                          color: Color(0xff0c2942),
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    )
                  ],
                ),
                Center(
                  child: Text(
                    widget.duration == null ?
                    '¡Minutos!': widget.duration.inMinutes.toString() + "m  " + widget.duration.inSeconds.toString() + "s",
                    textDirection: TextDirection.ltr,
                    style: TextStyle(
                      fontSize: 38,
                      color: Color(0xff0c2942),
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Container(
                          height: MediaQuery.of(context).size.height * 0.3,
                          width: MediaQuery.of(context).size.width * 0.3,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Colors.white,
                            border: Border.all(
                              color: Color(0xff0c2942),
                            ),
                          ),
                          child: Center(
                            child: Text('${widget.duration.inHours.toString()} Hr'),
                          ),
                        ),
                      ],
                    ),
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Container(
                          height: MediaQuery.of(context).size.height * 0.3,
                          width: MediaQuery.of(context).size.width * 0.3,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Colors.white,
                            border: Border.all(
                              color: Color(0xff0c2942),
                            ),
                          ),
                          child: Center(
                            child: Text('${widget.duration.inMinutes.toString()} m'),
                          ),
                        ),
                      ],
                    ),
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Container(
                          height: MediaQuery.of(context).size.height * 0.3,
                          width: MediaQuery.of(context).size.width * 0.3,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Colors.white,
                            border: Border.all(
                              color: Color(0xff0c2942),
                            ),
                          ),
                          child: Center(
                            child: Text('${widget.duration.inSeconds.toString()} s'),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
        )
    );

  }

}