import 'package:biblioapp/ui/ExplorerMode/ExplorerMode.dart';
import 'package:biblioapp/ui/ExplorerMode/Section.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import '../../data/moor_database.dart';

class InstructionsClass extends StatefulWidget {
  InstructionsClass({this.instructionSnapShot, this.init});
  final DateTime init;

  final DocumentSnapshot instructionSnapShot;

  @override
  _InstructionsClassState createState() => _InstructionsClassState();
}


class _InstructionsClassState extends State<InstructionsClass> {
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();
  PageController pageController;

  Widget _buildListItem(BuildContext context, String i, int index, int size){
    Set<int> _mySet = new Set();
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Column(
          children: [
            _mySet.contains(index) ? Text("hola"):
            Image(
              image: AssetImage('assets/images/escalera.png'),
              width: 60.0,
              height: 60.0,
            ),
          ],
        ),
        Expanded(
          child: InkWell(
            onTap: (){
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          SectionClass(
                            sectionSnapshot: widget.instructionSnapShot,
                            init: widget.init,
                          )
                  ),
                  ModalRoute.withName("/Section")
              );
            },
            child: Text(
              i,
              textDirection: TextDirection.ltr,
              textAlign: TextAlign.left,
              style: TextStyle(
                fontSize: 20,
                color: Color(0xff436b9a),
              ),
            ),
          ),
        ),
      ],
    );
  }

  @override
  void initState() {
    super.initState();
  }

  Future<bool> _onBackPressed() {
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return CupertinoAlertDialog(
            title: Text(
                "Salir del modo recorrido"
            ),
            content: Text(
                "¿Estas seguro de salir del modo recorrido?"
            ),
            actions: <Widget>[
              CupertinoDialogAction(
                child: Text("No"),
                onPressed: () {
                  Navigator.pop(context,false);
                },
              ),
              CupertinoDialogAction(
                child: Text("Si"),
                onPressed: () {
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              ExplorerMode(
                                title: 'GUIAPP',
                                sectionName: widget.instructionSnapShot.documentID,
                              )
                      ),
                      ModalRoute.withName("/FinishTourMode")
                  );
                },
              )
            ],
          );
        }
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text('GUIAPP Luis Angel'),
          backgroundColor: Color(0xff000000),
        ),
        body: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Image(
                image: AssetImage('assets/images/direccion-up.png'),
                width: 120.0,
                height: 120.0,
              ),
              Expanded(
                  child: ListView.builder(
                  itemExtent: 80.0,
                  itemCount: widget.instructionSnapShot.data["instrucciones"].length,
                  itemBuilder: (context, index) =>
                    _buildListItem(context, widget.instructionSnapShot.data["instrucciones"][index], index, widget.instructionSnapShot.data["instrucciones"].length),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}


class InstructionsClassWithoutConnection extends StatefulWidget {
  InstructionsClassWithoutConnection({this.itemSection, this.init});

  final DateTime init;

  final Section itemSection;

  @override
  _InstructionsClassWithoutConnection createState() => _InstructionsClassWithoutConnection();
}


class _InstructionsClassWithoutConnection extends State<InstructionsClassWithoutConnection> {
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();
  PageController pageController;

  Widget _buildListItem(BuildContext context, String i, int index, int size){
    Set<int> _mySet = new Set();
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Column(
          children: [
            _mySet.contains(index) ? Text("hola"):
            Image(
              image: AssetImage('assets/images/escalera.png'),
              width: 60.0,
              height: 60.0,
            ),
          ],
        ),
        Expanded(
          child: InkWell(
            onTap: (){
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => SectionClassWithoutConnection(itemSection: widget.itemSection,init: widget.init)),
              );
            },
            child: Text(
              i,
              textDirection: TextDirection.ltr,
              textAlign: TextAlign.left,
              style: TextStyle(
                fontSize: 20,
                color: Color(0xff436b9a),
              ),
            ),
          ),
        ),
      ],
    );
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('GUIAPP Luis Angel'),
        backgroundColor: Color(0xff000000),
      ),
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Image(
              image: AssetImage('assets/images/direccion-up.png'),
              width: 120.0,
              height: 120.0,
            ),
            Expanded(
              child: ListView.builder(
                itemExtent: 80.0,
                itemCount: widget.itemSection.instructionSection.split('-').length,
                itemBuilder: (context, index) =>
                    _buildListItem(context, widget.itemSection.instructionSection.split('-')[index], index,widget.itemSection.instructionSection.split('-').length),
              ),
            ),
          ],
        ),
      ),
    );
  }
}