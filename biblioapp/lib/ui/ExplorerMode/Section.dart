import 'package:biblioapp/models/user.dart';
import 'package:biblioapp/services/database.dart';
import 'package:biblioapp/ui/ExplorerMode/ExpModeCloseSections.dart';
import 'package:biblioapp/ui/ExplorerMode/FinishTour.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:moor_flutter/moor_flutter.dart' hide Column;
import 'package:provider/provider.dart';
import '../../data/moor_database.dart';
import 'package:biblioapp/ui/ExplorerMode/vrai.dart';
import '../CommentPlace.dart';
import 'package:biblioapp/ui/ExplorerMode/ExpModeSections.dart';
import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';

import 'ExplorerMode.dart';


  class SectionClass extends StatefulWidget {
    SectionClass({this.sectionSnapshot, this.ds, this.init});
    final DateTime init;

    final DocumentSnapshot sectionSnapshot;

    DocumentSnapshot ds;

    @override
    _SectionClassState createState() => _SectionClassState();
  }


  class _SectionClassState extends State<SectionClass> {
    PageController pageController;

    void _initDocumentSection() async {
      var d = await Firestore.instance.collection("secciones").document(widget.sectionSnapshot.documentID).get().then(
              (ds) => ds
      ).catchError((e) => print(e));
      setState(() {
        widget.ds = d;
      });
    }

    @override
    void initState() {
      super.initState();
      _initDocumentSection();
    }

    Future<bool> _onBackPressed() {
      return showDialog(
          context: context,
          barrierDismissible: true,
          builder: (BuildContext context){
            return CupertinoAlertDialog(
              title: Text(
                  "Salir del modo recorrido"
              ),
              content: Text(
                  "¿Estas seguro de salir del modo recorrido?"
              ),
              actions: <Widget>[
                CupertinoDialogAction(
                  child: Text("No"),
                  onPressed: () {
                    Navigator.pop(context,false);
                  },
                ),
                CupertinoDialogAction(
                  child: Text("Si"),
                  onPressed: () {
                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                ExplorerMode(
                                  title: 'GUIAPP',
                                  sectionName: widget.sectionSnapshot.documentID,
                                )
                        ),
                        ModalRoute.withName("/FinishTourMode")
                    );
                  },
                )
              ],
            );
          }
      );
    }

    @override
    Widget build(BuildContext context) {
      final user = Provider.of<User>(context);
      return WillPopScope(
        onWillPop: _onBackPressed,
        child: Scaffold(
          appBar: AppBar(
            title: Text('GUIAPP Luis Angel'),
            backgroundColor: Color(0xff000000),
          ),
          body: SingleChildScrollView(
            child: Stack(
              children: [
                Positioned(
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          SizedBox(height: 150),
                          Image(
                            image: AssetImage('assets/images/posicion-blue.png'),
                            width: 70.0,
                            height: 70.0,
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                'Ubicación Actual',
                                textDirection: TextDirection.ltr,
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  fontSize: 24,
                                  color: Color(0xff052542),
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Text(
                                widget.sectionSnapshot.documentID,
                                textDirection: TextDirection.ltr,
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  fontSize: 20,
                                  color: Color(0xff052542),
                                ),
                              ),
                            ],
                          )

                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          widget.ds == null || widget.ds.data == null ?
                          Image(
                            image: AssetImage('assets/images/biblioteca_luis_angel_arango3.jpg'),
                            width: 300.0,
                            height: 200.0,
                          )
                              :
                          CachedNetworkImage(
                            progressIndicatorBuilder: (context, url, progress) =>
                                CircularProgressIndicator(
                                  value: progress.progress,
                                ),
                            imageUrl:(widget.ds.data["image"]),
                            width: 300.0,
                            height: 200.0,
                          )

                        ],
                      ),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Expanded(
                              child: Center(
                                child: Text(
                                  widget.sectionSnapshot.documentID,
                                  textDirection: TextDirection.ltr,
                                  style: TextStyle(
                                    fontSize: 32,
                                    color: Color(0xff052542),
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                          ]
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                            child: Center(
                              child: Text(
                                widget.ds == null ? "" :
                                widget.ds.data == null ? "": widget.ds.data["descripcion"],
                                textDirection: TextDirection.ltr,
                                style: TextStyle(
                                  fontSize: 20,
                                  color: Color(0xff052542),
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),

                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Center(
                            child: ButtonTheme(
                              buttonColor: Color(0xff010201),
                              minWidth: 300.0,
                              height: 50.0,
                              child: RaisedButton(
                                  child: Text(
                                    'Agregar comentario',
                                    style: TextStyle(
                                      fontSize: 20,
                                      color: Colors.white,
                                    ),
                                  ),
                                  onPressed: () {

                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(builder: (context) => CommentScreen(seccionId: widget.sectionSnapshot.documentID)),
                                    );
                                  }
                              ),
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          floatingActionButtonLocation:
          FloatingActionButtonLocation.centerDocked,
          floatingActionButton: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  FloatingActionButton(
                    heroTag: "endTourMode",
                    onPressed: () {
                      showDialog(
                          context: context,
                          barrierDismissible: true,
                          builder: (BuildContext context){
                            return CupertinoAlertDialog(
                              title: Text(
                                  "Salir del modo recorrido"
                              ),
                              content: Text(
                                  "¿Estas seguro de salir del modo recorrido?"
                              ),
                              actions: <Widget>[
                                CupertinoDialogAction(
                                  child: Text("No"),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                ),
                                CupertinoDialogAction(
                                  child: Text("Si"),
                                  onPressed: () {
                                    var now = DateTime.now();
                                    print("section init ${widget.init}");
                                    print("section $now");
                                    final difference = now.difference(widget.init);
                                    print("difference $difference");
                                    final dao = Provider.of<JourneyDao>(context);
                                    final journey = JourneysCompanion(
                                        startDateJourney: Value(widget.init),
                                        endDateJourney: Value(now),
                                        durationJourney: Value(difference.inSeconds),
                                    );
                                    print("inserting journey");
                                    dao.insertJourney(journey);
                                    print("usr/${user.uid}");
                                    Firestore.instance
                                        .document("usr/${user.uid}")
                                        .collection("secciones").document("${widget.sectionSnapshot.documentID}")
                                        .setData({
                                          "cantidadVecesVisitada": FieldValue.increment(1),
                                          "duracionAcumuladaMillis": FieldValue.increment(difference.inMilliseconds),
                                          "visitada": true,
                                        },);
                                    Navigator.pushAndRemoveUntil(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                FinishTourMode(
                                                  section: widget.sectionSnapshot.documentID,
                                                  duration: difference,
                                                )
                                        ),
                                        ModalRoute.withName("/FinishTourMode")
                                    );
                                  },
                                )
                              ],
                            );
                          }
                      );
                    },
                    child: Icon(Icons.cancel),
                  ),
                  FloatingActionButton(
                    heroTag: "startTourMode",
                    onPressed: () async {
                          try {
                            final result = await InternetAddress.lookup('google.com');
                            if (result.isNotEmpty && result[0].rawAddress.isNotEmpty && widget.ds != null) {
                              print('connected');
                              print(widget.ds);
                              Navigator.pushAndRemoveUntil(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          ExpModeCloseSections(
                                            actualSection: widget.ds.documentID,
                                            init: widget.init,
                                          )
                                  ),
                                  ModalRoute.withName("/ExpModeCloseSections")
                              );
                            }
                          } on SocketException catch (_) {
                            print('not connected');
                            Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => ExpModeSectionsWithoutConnection ())
                            );
                          }

                    },
                    child: Icon(Icons.directions_run),
                  )
                ],
              ),
            )
        ),
      );

    }
  }

  class SectionClassWithoutConnection extends StatefulWidget {
    SectionClassWithoutConnection({this.itemSection, this.init});

    final DateTime init;

    final Section itemSection;

    @override
    _SectionClassWithoutConnectionState createState() => _SectionClassWithoutConnectionState();
  }


  class _SectionClassWithoutConnectionState extends State<SectionClassWithoutConnection> {
    GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();
    PageController pageController;

    @override
    void initState() {
      super.initState();
    }

    @override
    Widget build(BuildContext context) {
      return Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
            title: Text('GUIAPP Luis Angel'),
            backgroundColor: Color(0xff000000),
          ),
          body: SingleChildScrollView(
            child:Stack(
              children: [
                Positioned(
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          SizedBox(height: 150),
                          Image(
                            image: AssetImage('assets/images/posicion-blue.png'),
                            width: 70.0,
                            height: 70.0,
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                'Ubicación Actual',
                                textDirection: TextDirection.ltr,
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  fontSize: 24,
                                  color: Color(0xff052542),
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Text(
                                widget.itemSection.sectionName,
                                textDirection: TextDirection.ltr,
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  fontSize: 20,
                                  color: Color(0xff052542),
                                ),
                              ),
                            ],
                          )

                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image(
                            image: AssetImage('assets/images/biblioteca_luis_angel_arango3.jpg'),
                            width: 300.0,
                            height: 200.0,
                          ),
                        ],
                      ),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Center(
                              child: Text(
                                widget.itemSection.sectionName,
                                textDirection: TextDirection.ltr,
                                style: TextStyle(
                                  fontSize: 32,
                                  color: Color(0xff052542),
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ]
                      ),
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: Center(
                              child: Text(
                                widget.itemSection.descriptionSection == null ? "": widget.itemSection.descriptionSection,
                                style: TextStyle(
                                  fontSize: 20,
                                  color:Color(0xff052542),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Center(
                            child: ButtonTheme(
                              buttonColor: Color(0xff010201),
                              minWidth: 300.0,
                              height: 50.0,
                              child: RaisedButton(
                                  child: Text(
                                    'Agregar comentario',
                                    style: TextStyle(
                                      fontSize: 20,
                                      color: Colors.white,
                                    ),
                                  ),
                                  onPressed: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(builder: (context) => CommentPlace()),
                                    );
                                  }
                              ),
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          floatingActionButtonLocation:
          FloatingActionButtonLocation.centerDocked,
          floatingActionButton: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                FloatingActionButton(
                  heroTag: "endTourMode",
                  onPressed: () {
                    showDialog(
                        context: context,
                        barrierDismissible: true,
                        builder: (BuildContext context){
                          return CupertinoAlertDialog(
                            title: Text(
                                "Salir del modo recorrido"
                            ),
                            content: Text(
                                "¿Estas seguro de salir del modo recorrido?"
                            ),
                            actions: <Widget>[
                              CupertinoDialogAction(
                                child: Text("No"),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                              CupertinoDialogAction(
                                child: Text("Si"),
                                onPressed: () {
                                  var now = DateTime.now();
                                  print("section init ${widget.init}");
                                  print("section $now");
                                  final difference = now.difference(widget.init);
                                  print("difference $difference");
                                  final dao = Provider.of<JourneyDao>(context);
                                  final journey = JourneysCompanion(
                                    startDateJourney: Value(widget.init),
                                    endDateJourney: Value(now),
                                    durationJourney: Value(difference.inSeconds),
                                  );
                                  print("inserting journey");
                                  dao.insertJourney(journey);
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(builder: (context) => FinishTourMode(duration: difference,))
                                  );
                                },
                              )
                            ],
                          );
                        }
                    );
                  },
                  child: Icon(Icons.cancel),
                ),
                FloatingActionButton(
                  heroTag: "startTourMode",
                  onPressed: () {
                    print(widget.itemSection.sectionName);
                    Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => ExpModeSections())
                    );
                  },
                  child: Icon(Icons.directions_run),
                )
              ],
            ),
          )
      );

    }
  }
