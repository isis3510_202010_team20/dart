
import 'package:biblioapp/services/auth.dart';
import 'package:biblioapp/services/database.dart';
import 'package:biblioapp/shared/loading.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import "dart:async";
import 'package:firebase_auth/firebase_auth.dart';
import 'package:provider/provider.dart';
import 'package:biblioapp/models/User.dart';
import 'package:biblioapp/models/user.dart';
import 'package:biblioapp/shared/constants.dart';
import 'package:rating_bar/rating_bar.dart';


class CommentScreen extends StatefulWidget {
  final String seccionId;
  
  const CommentScreen({this.seccionId}); 
  @override
  _CommentScreenState createState() => _CommentScreenState(
      seccionID: this.seccionId,
  );
}

class _CommentScreenState extends State<CommentScreen> {
  final String seccionID;
  String userID;
  final List<int> lik = [0,1,2,3,4,5]; // form values
  int _currentLik = 0;



  final TextEditingController _commentController = TextEditingController();

  _CommentScreenState({this.seccionID, this.userID}); 

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.grey,
        title: Center(
          child: Text(
            "Comments $seccionID",
            style: TextStyle(color: Colors.white),
          ),
        ),
      ),
      body: buildPage(),
    );
  }

  Widget buildPage() {
    
    final user = Provider.of<User>(context);
    
    return StreamBuilder<UserData>(
      stream: DatabaseService(uid: user.uid).userData,
      // ignore: missing_return
      builder: (context, snapshot) {
        print("snapshooot ${snapshot.data}");
        if(snapshot.hasData){
          print("snapshooot ${snapshot.data}");
          UserData userData = snapshot.data;
          //print(userData.name);
          //print(userData.name);
          userID = userData.name;
          return Column(
            children: [
              Expanded(
                child:
                  buildComments(),
                  
              ),
              Divider(),
              ListTile(
                subtitle: DropdownButtonFormField(
                    decoration: textInputDecoration.copyWith(hintText: 'Like'),
                    value: _currentLik,   
                     items: lik.map((like) {
                      return DropdownMenuItem(
                        value: like,
                        child: Text('$like '),
                      );
                    }).toList(),
                    onChanged: (val) => setState(() => _currentLik = val ),
                  ),
                
                title: TextFormField(
                  controller: _commentController,
                  cursorColor: Colors.grey,
                  decoration: InputDecoration(labelText: 'Write a comment...',),
                  onFieldSubmitted: addComment,
                ),
                trailing: OutlineButton(onPressed: (){addComment(_commentController.text); }, borderSide: BorderSide() , child: Text("Post"),),
                
                
              ),
              

            ],
          );}else{
            Loading();
          }
      }
    );

  }


  Widget buildComments() {
    return FutureBuilder<List<Comment>>(
        future: getComments(),
        builder: (context, snapshot) {
          print(snapshot.data);
          if (!snapshot.hasData)
            return Container(
                alignment: FractionalOffset.center,
                child: CircularProgressIndicator());
          return ListView(
            children: snapshot.data,
          );
        });
  }
    

  Future<List<Comment>> getComments() async {
    List<Comment> comments = [];

    QuerySnapshot data = await Firestore.instance
        .document("seccion/$seccionID")
        .collection("Comentarios")
        .getDocuments();
    data.documents.forEach((DocumentSnapshot doc) {
      comments.add(Comment.fromDocument(doc));
    });

    return comments;
  }

  addComment(String comment) {
    _commentController.clear();
    //buildComments();
    if (comment != '')
    Firestore.instance
        .document("seccion/$seccionID")
        .collection("Comentarios")
        .add({
      "username": userID,
      "likes": _currentLik,
      "comment": comment,
      

    });
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => CommentScreen(seccionId: seccionID)),
    );
   
  }
  addLike2(DocumentSnapshot docsnap) {
    
    Firestore.instance
        .document("seccion/$seccionID")
        .collection("Comentarios").document(docsnap.documentID)
        
        .updateData({'likes':!docsnap['likes']});
        print(docsnap.documentID);
    
  }

  Future<void> updateCommentData(String username, int likes, String comment, DocumentSnapshot docsnap) async {
    return await Firestore.instance
      .document("seccion/$seccionID/$docsnap.documentID")
      .setData({
        'username': username,
        'likes' : likes,
        'comment': comment,
        
      });
  }
}


class Comment extends StatelessWidget {
  final String username;
  //final String userID;
  int likes;

  final String comment;
  //final int timestamp;

  Comment(
      {this.username,
      
      this.likes,
      this.comment,
      });

  factory Comment.fromDocument(DocumentSnapshot document) {
    return Comment(
      username: document['username'],
      //userId: document['userId'],
      likes: document['likes'],
      comment: document["comment"],
      
    );
  }
    addLike() {
      //final reference = Firestore.instance.collection('seccion/$seccionID');
    //buildComments();
      this.likes += 1;
  }
   


  @override
  Widget build(BuildContext context) {   
        
        return Column(
          children: <Widget>[
            ListTile(

              title: Text(username ?? ''),
              subtitle: Text(comment),

              leading: Icon(Icons.person, color: Colors.grey, size:36),
              trailing:
                        SizedBox(
                            width: 120,
                            height: 40,
                                                  child: RatingBar.readOnly(
            initialRating: likes.toDouble() ?? 0.0,
            
            
            filledIcon: Icons.star,
            filledColor: Colors.yellow,
            emptyIcon: Icons.star_border,
            size: 20,
          ),
                        ),

  //             Container(
  //               // heightFactor: 8,
  //               // widthFactor: 30
  //               children[
  //                 Text("$likes"),
  //                 Icon(Icons.star),

  //               ]
             
  //               // for (i in likes){

  //               // }

                
  //               // child:  RatingBar.readOnly(
  //               //   initialRating: likes.toDouble() ?? 0.0,
  //               //   filledIcon: Icons.star,
  //               //   emptyIcon: Icons.star_border,
  // ),
              ),
  //  //           Text("$likes" ?? 0),
              
              
            
            
            Divider(),
          ],
        );
  }
}