import 'package:biblioapp/models/user.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:biblioapp/models/SubscriberStatistics.dart';

class StatisticsClass extends StatefulWidget {
  final String title;

  const StatisticsClass({Key key, this.title}) : super(key: key);

  @override
  StatisticsClassState createState() => StatisticsClassState();
}

class StatisticsClassState extends State<StatisticsClass> {
  PageController pageControllerOne;
  PageController pageControllerTwo;
  final setSection = <String>{};
  List<String> imagesOne = [
    'assets/images/logo.jpeg'
  ];
  List<String> imagesTwo = [
    'assets/images/logo.jpeg'
  ];

  _buildPageSectionItem(BuildContext context, DocumentSnapshot document){
    print("${document.documentID}");
    print("${setSection.contains(document.documentID)}");
    return setSection.contains(document.documentID) ?
    Container(
        margin: EdgeInsets.all(5),
        child: Stack(
            children: [
              Center(
                child: Image(
                  image: AssetImage('assets/images/biblioteca_luis_angel_arango3.jpg'),
                  fit: BoxFit.cover,
                ),
              ),
              Center(
                child: Text(
                  document.documentID,
                  style: TextStyle(fontSize: 19.0, color: Colors.blue, backgroundColor: Colors.white),
                ),
              )
            ]
        )
    ): Container(
        margin: EdgeInsets.all(5),
        child: Stack(
            children: [
              Center(
                child: Image(
                  image: AssetImage('assets/images/biblioteca_luis_angel_arango3.jpg'),
                  fit: BoxFit.cover,
                ),
              ),
              Center(
                child: Text(
                  document.documentID,
                  style: TextStyle(fontSize: 19.0, backgroundColor: Colors.blue, color: Colors.white),
                ),
              )
            ]
        )
    );
  }

  _buildChart(BuildContext context, AsyncSnapshot snapshot){
    final List<SubscriberSeries> data = [];
    for(var doc in snapshot.data.documents){
      data.add(
        SubscriberSeries(
          year: doc.documentID,
          subscribers: doc.data["cantidadVecesVisitada"],
          barColor: charts.ColorUtil.fromDartColor(Colors.blue),
        ),
      );
      setSection.add(doc.documentID);
    }
    return SubscriberChart(data: data,);
  }


  @override
  void initState() {
    super.initState();
    pageControllerTwo = PageController(initialPage: 1, viewportFraction: 0.6);

  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);

    return Scaffold(
        backgroundColor: Color(0xff000000),
        appBar: AppBar(
            title: Text('GUIAPP Luis Angel'),

            elevation: 0.0,
        ),
        body: SingleChildScrollView(
          child: SizedBox(
            height: MediaQuery.of(context).size.height,
            child: Container(
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topRight,
                        end: Alignment.bottomLeft,
                        colors: [
                          Color(0xff0e263d),
                          Color(0xff2e5680),
                        ]
                    )
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        SizedBox(height: 150),
                        Image(
                          image: AssetImage('assets/images/statistics.png'),
                          width: 70.0,
                          height: 70.0,
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Center(
                              child: Text(
                                'Estadísticas',
                                textDirection: TextDirection.ltr,
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  fontSize: 24,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                    SizedBox(
                      child: StreamBuilder(
                        stream: Firestore.instance.collection("usr/${user.uid}/secciones").snapshots(),
                        builder: (context, snapshot) {
                          if(!snapshot.hasData) return const Text("Cargando Visualización");
                          return _buildChart(context, snapshot);
                        }
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 20.0, bottom: 20.0, left: 20.0),
                      alignment: Alignment.centerLeft,
                      child: Row(
                        children: <Widget>[
                          Column(
                            children: <Widget>[
                              Text(
                                'Secciones visitadas',
                                style: TextStyle(
                                  fontSize: 15.0,
                                  color: Colors.blue, backgroundColor: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Text(
                                'Secciones por visitar',
                                style: TextStyle(
                                  fontSize: 15.0,
                                  backgroundColor: Colors.blue, color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                    Expanded(
                      child: StreamBuilder(
                          stream: Firestore.instance.collection("secciones").snapshots(),
                          builder: (context, snapshot){
                            if(!snapshot.hasData) return const Text("Cargando secciones no visitadas");
                            return PageView.builder(
                              itemCount: snapshot.data.documents.length,
                              itemBuilder: (context, index) => _buildPageSectionItem(context, snapshot.data.documents[index]),
                            );
                          }),
                    ),

                  ],
                )
            ),
          ),
        )
    );
  }
  imageSliderOne(int index){
    return AnimatedBuilder(
      animation: pageControllerOne,
      builder: (context, widget){
        double value = 1;
        if(pageControllerOne.position.haveDimensions){
          value = pageControllerOne.page - index;
          value = (1 - (value.abs() * 0.3)).clamp(0.0, 1.0);
        }

        return Container(
          alignment: Alignment.topCenter,
          height: 200,
          child: widget,
        );
      },
      child: Container(
          margin: EdgeInsets.all(10),
          child: Image(
            image: AssetImage(imagesOne[index]),
            fit: BoxFit.cover,
          )//Image.network(images[index],fit: BoxFit.cover,),
      ),
    );
  }
  imageSliderTwo(int index){
    return AnimatedBuilder(
      animation: pageControllerTwo,
      builder: (context, widget){
        double value = 1;
        if(pageControllerTwo.position.haveDimensions){
          value = pageControllerTwo.page - index;
          value = (1 - (value.abs() * 0.3)).clamp(0.0, 1.0);
        }

        return Container(
          alignment: Alignment.topCenter,
          height: 200,
          child: widget,
        );
      },
      child: Container(
          margin: EdgeInsets.all(10),
          child: Image(
            image: AssetImage(imagesTwo[index]),
            fit: BoxFit.cover,
          )//Image.network(images[index],fit: BoxFit.cover,),
      ),
    );
  }
}
