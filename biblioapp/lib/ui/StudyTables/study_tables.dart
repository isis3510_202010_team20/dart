import 'dart:async';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:biblioapp/data/moor_database.dart';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';


class StudyTables extends StatelessWidget {

  Widget _buildListItem(BuildContext context, DocumentSnapshot document){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        GestureDetector(
          onTap: () async {
            print("study table");
            print(document.documentID);
            await Firestore.instance.collection("mesas")
                .document(document.documentID)
                .updateData({'ocupado':!document['ocupado']});
          },
          child: document['ocupado'] ?
          Text(
            document['codigo']+" disponible",
            textDirection: TextDirection.ltr,
            textAlign: TextAlign.left,
            style: TextStyle(
              fontSize: 20,
              color: Color(0xff436b9a),
            ),
          )
              :
          Text(
            document['codigo']+ " ocupado",
            textDirection: TextDirection.ltr,
            textAlign: TextAlign.left,
            style: TextStyle(
              fontSize: 20,
              color: Color(0xff995853),
            ),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
//      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('GUIAPP Luis Angel'),
        backgroundColor: Color(0xff000000),
      ),
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image(
                  image: AssetImage('assets/images/study.png'),
                  width: 70.0,
                  height: 70.0,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      'Mesas de estudio',
                      textDirection: TextDirection.ltr,
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontSize: 24,
                        color: Color(0xff0e263d),
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                )
              ],
            ),
            Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                    child: Center(
                      child: Text(
                        'Aqui puedes ver las mesas de estudio, oprime en cada una de ellas para reservarlas',
                        textDirection: TextDirection.ltr,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontSize: 20,
                          color: Color(0xff0e263d),
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  )
                ]
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: 20,
                  height: 20,
                  decoration: BoxDecoration(
                    color: Color(0xff436b9a),
                    shape: BoxShape.circle
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      'Disponibles',
                      textDirection: TextDirection.ltr,
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontSize: 20,
                        color: Color(0xff0e263d),
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
//                Spacer(flex: 8,),
                Container(
                  width: 20,
                  height: 20,
                  decoration: BoxDecoration(
                      color: Color(0xff995853),
                      shape: BoxShape.circle
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      'Ocupadas',
                      textDirection: TextDirection.ltr,
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontSize: 20,
                        color: Color(0xff995853),
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                )
              ],
            ),
            Expanded(
              child: StreamBuilder(
                  stream: Firestore.instance.collection("mesas").snapshots(), //Firestore.instance.collection("secciones").snapshots()
                  builder: (context, snapshot) {
                    if (!snapshot.hasData) return const Text("Cargando Mesas Disponibless");
                    return ListView.builder(
                      itemExtent: 80.0,
                      itemCount: snapshot.data.documents.length,
                      itemBuilder: (context, index) =>
                          _buildListItem(context, snapshot.data.documents[index]),
                    );
                  }),
            )
          ],
        ),
      ),
    );

  }
}

