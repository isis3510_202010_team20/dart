import 'package:biblioapp/services/database.dart';
import 'package:biblioapp/shared/loading.dart';

import '../shared/constants.dart';
import 'package:flutter/material.dart';
import '../models/user.dart';
import 'package:provider/provider.dart';

class SettingsForm extends StatefulWidget {
  @override
  _SettingsFormState createState() => _SettingsFormState();
}

class _SettingsFormState extends State<SettingsForm> {

  final _formKey = GlobalKey<FormState>();
  final List<int> age = [0, 10, 20, 30, 40,50, 60, 70, 80, 90]; // form values
  String _currentName;
  int _currentAge;

  @override
  Widget build(BuildContext context) {

    final user = Provider.of<User>(context);

    return StreamBuilder<UserData>(
      stream: DatabaseService(uid: user.uid).userData,
      builder: (context, snapshot) {
        if(snapshot.hasData){

          UserData userData = snapshot.data;
          return Form(
          key: _formKey,
          child: Column(
            children: <Widget>[
              Text(
                'Update your user settings.',
                style: TextStyle(fontSize: 18.0),
              ),
              SizedBox(height: 20.0),
              TextFormField(
                initialValue: _currentName ?? userData.name,
                decoration: textInputDecoration,//.copyWith(hintText:'Name'),
                validator: (val) => val.isEmpty ? 'Please enter a name' : null,
                onChanged: (val) => setState(() => _currentName = val),
              ),
              SizedBox(height: 10.0),
              Wrap(
                          children: [
                  DropdownButtonFormField(
                    decoration: textInputDecoration.copyWith(hintText: 'Age'),
                    value: _currentAge ?? userData.age,   //use of stream builder to store the users'value (more evaluate)
                     items: age.map((age) {
                      return DropdownMenuItem(
                        value: age,
                        child: Text('$age '),
                      );
                    }).toList(),
                    onChanged: (val) => setState(() => _currentAge = val ),
                  ),
                ],
              ), 
              SizedBox(height: 10.0),
              RaisedButton(
                color: Colors.blue[400],
                child: Text(
                  'Update',
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () async {
                  if(_formKey.currentState.validate()){
                    await DatabaseService(uid: user.uid).updateUserData(
                    _currentName ?? userData.name,
                    _currentAge ?? userData.age,

                  );
                  Navigator.pop(context);
                  }
                 
                  
                }
              ),
            ],
          ),
        );

        }else{
          return Loading();

        }
        
      }
    );
  }
}