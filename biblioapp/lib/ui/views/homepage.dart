import 'package:biblioapp/ui/ExplorerMode/ExplorerMode.dart';
import 'package:flutter/material.dart';
class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text(widget.title),
        ),
        body: new Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topRight,
                    end: Alignment.bottomLeft,
                    colors: [
                      Color(0xff0e263d),
                      Color(0xff2e5680),
                    ]
                )
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(

                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image(
                      image: AssetImage('assets/images/posicion.png'),
                      width: 120.0,
                      height: 120.0,
                    ),
                  ],

                ),
                Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Center(
                        child: Text(
                          'LaLuisAngel',
                          textDirection: TextDirection.ltr,
                          style: TextStyle(
                            fontSize: 32,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ]
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(height: 300),
                    Center(
                      child: RaisedButton(
                          child: Text('ENTER'),
                          onPressed: () {
                            Navigator.push(
                              context,
                              //MaterialPageRoute(builder: (context) => FirstRoute()),
                              MaterialPageRoute(builder: (context) => ExplorerMode(title: 'GUIAPP', sectionName: "Entrada",)),
                            );
                          }
                      ),
                    )
                  ],
                )
              ],
            )

        )
    );
  }
}





