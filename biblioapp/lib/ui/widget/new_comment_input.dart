import 'package:biblioapp/models/user.dart';
import 'package:biblioapp/services/database.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:moor_flutter/moor_flutter.dart' hide Column;
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';
import 'package:firebase_database/firebase_database.dart';
import '../../data/moor_database.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class NewCommentInput extends StatefulWidget {
  //NewCommentInput({this.doc_cur});
  //final String = doc_cur; 
 
  //final dbRef = FirebaseDatabase.instance.reference().child("${doc_cur}/data/commentarios");

  //const NewCommentInput({
    //Key key,
  //}) : super(key: key);

  @override
  _NewCommentInputState createState() => _NewCommentInputState();
}

class _NewCommentInputState extends State<NewCommentInput> {
  TextEditingController controller;

  @override
  void initState() {
    super.initState();
    controller = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    
    return Container(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          _buildTextField(context),
        ],
      ),
    );
  }

  Expanded _buildTextField(BuildContext context) {
    final user = Provider.of<User>(context);
    return Expanded(
      child: StreamBuilder<UserData>(
        stream: DatabaseService(uid: user.uid).userData,
        builder: (context, snapshot) {
          return TextField(
            controller: controller,
            decoration: InputDecoration(hintText: 'Comment Name'),
            onSubmitted: (inputName) {
              final dao = Provider.of<CommentDao>(context);
              final comment = CommentsCompanion(
                description: Value(inputName),
              );
              dao.insertComment(comment);
              resetValuesAfterSubmit();
            },
          );
        }
      ),
    );
  }


  void resetValuesAfterSubmit() {
    setState(() {
      controller.clear();
    });
  }
}