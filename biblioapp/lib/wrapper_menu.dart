import 'package:biblioapp/ui/ExplorerMode/ExplorerMode.dart';
import'package:flutter/material.dart';
import 'package:biblioapp/models/user.dart';
import 'package:provider/provider.dart';
import 'authentificate/auth.dart';

class Wrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);

    if (user == null){
      return Auth();

    }else {
      return ExplorerMode(title: 'GUIAPP', sectionName: "Entrada",);
      //return MyHomePage(title: 'BiblioApp');
    }
    //return Auth();
  }
}
